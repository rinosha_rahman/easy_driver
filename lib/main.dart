 import 'package:eaasy_driver/screens/app.dart';
import 'package:eaasy_driver/screens/create.dart';
import 'package:eaasy_driver/screens/forget.dart';
import 'package:eaasy_driver/screens/login.dart';
import 'package:eaasy_driver/screens/splash.dart';
import 'package:eaasy_driver/screens/scan.dart';
import 'package:flutter/material.dart';
import 'screens/help.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Easy Tickets',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
          primaryColor: Colors.black,
          fontFamily: "worksans",
          floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: Colors.black87,
          )),
       home: SplashPage(),
      routes: <String, WidgetBuilder>{
        "/login": (BuildContext context) => new LoginPage(),
         "/app": (BuildContext context) => new AppPage(),
        "/help": (BuildContext context) => new HelpPage(),
         "/create": (BuildContext context) => new CreatePage(),
         "/forget": (BuildContext context) => new ForgetPage(),
         "/scan": (BuildContext context) => new ScanPage(),
       },
    );
  }
}
