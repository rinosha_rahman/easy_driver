import 'package:flutter/material.dart';

class HelpPage extends StatefulWidget {
  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(),
      body: ListView(
        children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 20)),
          Image.asset("assets/images/logo.png"),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20),
            child: Text(
              "Easy tickets is a travel based application in which the current public transportation is made easier. With this app the users can use their identification number as wells bus identification number using the mobile camera and automate the travel process. With an in-built wallet in the app, the users could store the cash and use it while he/she use the city public transportation system.",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 25, fontWeight: FontWeight.w300, height: 1.5),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 30)),
          Text(
            "hello@easytickets.com",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20,color: Colors.blue)
          ),
        ],
      ),
    );
  }

  AppBar mainAppBar() {
    return AppBar(
      title: Text("Help & Support"),
      actions: <Widget>[
        IconButton(
            icon: Icon(Icons.help),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed("/help");
            }),
        IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed("/login");
            }),
      ],
    );
  }
}
