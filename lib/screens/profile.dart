import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Expanded(
          // max space
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 25),
            child: pageView(context), //function
          ),
        ),
      ]),
    );
  }

  SingleChildScrollView pageView(context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Card(
            color: Color.fromRGBO(255, 255, 255, 1.0),
            margin: EdgeInsets.all(10),
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.all(15),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  TextField(
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "First Name", hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Last Name", hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Username", hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    // obscureText: true,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Address Line 1",
                        hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Address Line 2",
                        hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    // obscureText: true,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Mobile Number",
                        hasFloatingPlaceholder: true),
                  ),
                  Container(
                    height: 15,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                      color: Colors.black87,
                      onPressed: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            "/login", ModalRoute.withName('/'));
                      },
                      child: Text(
                        "Update Profile",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            color: Color.fromRGBO(255, 255, 255, 1.0),
            margin: EdgeInsets.all(10),
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.all(15),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  TextField(
                    obscureText: true,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "New Password",
                        hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    obscureText: true,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Current Password",
                        hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    obscureText: true,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Confirm Password",
                        hasFloatingPlaceholder: true),
                  ),
                  Container(
                    height: 15,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                      color: Colors.black87,
                      onPressed: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            "/login", ModalRoute.withName('/'));
                      },
                      child: Text(
                        "Change Password",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
