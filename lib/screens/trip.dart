import 'package:flutter/material.dart';

class TripPage extends StatefulWidget {
  @override
  _TripPageState createState() => _TripPageState();
}

class _TripPageState extends State<TripPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 5),
            child: pageView(context),
          ),
        ),
      ]),
    );
  }

  Card itemList(
      String date, String time, String amount, String to, String from) {
    return Card(
      margin: EdgeInsets.only(left: 10, right: 10, top: 5),
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  amount,
                  style: TextStyle(fontWeight: FontWeight.w900, fontSize: 20),
                ),
                Text(
                  date + " " + time,
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 10)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "From : " + from,
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 12),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 5)),
                Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "To : " + to,
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 12),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  SingleChildScrollView pageView(context) {
    return SingleChildScrollView(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Text(
                  "INR 32.00",
                  style: TextStyle(fontWeight: FontWeight.w900, fontSize: 30),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 0),
                child: Text("Location X (5th stop)"),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0, bottom: 10),
                child: Text(
                  "42 Passengers",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              )
            ],
          ),
        ),
        Divider(),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
        itemList("25-02-2020", "2:30 PM", "INR 250.00",
            "Location 2, Place 2, City 2", "Location 1, Place 1, City 1"),
      ],
    ));
  }
}
