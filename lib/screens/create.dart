import 'package:flutter/material.dart';

class CreatePage extends StatefulWidget {
  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(),
      body: Column(children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 10),
            child: pageView(context),
          ),
        ),
        masterButton(),
      ]),
    );
  }

  SingleChildScrollView pageView(context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          itemBox("Your Name"),
          itemBox("Mobile Number"),
          itemBox("Email Address"),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20,top:20),
            child: Text("Please contact the administrator to complete the registration process and create a new account. \nPlease follow the rules and regulations.",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 15, fontWeight: FontWeight.w300, height: 1.5,color: Colors.black54),
            ),
          ),
          // DESIGN ITEMS GOES HERE
        ],
      ),
    );
  }

  Center itemBox(String title) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
        child: new TextField(
          decoration: new InputDecoration(labelText: title),
        ),
      ),
    );
  }

  SizedBox masterButton() {
    return SizedBox(
      width: double.infinity,
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(0.0))),
        height: 50,
        color: Colors.black87,
        child: Text(
          'SEND REQUEST',
          style: TextStyle(
              color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600),
        ),
        
        onPressed: () {
          Navigator.of(context)
              .pushNamedAndRemoveUntil("/login", ModalRoute.withName('/'));
        },
      ),
    );
  }

  AppBar mainAppBar() {
    return AppBar(
      title: Text("Send Request"),
    );
  }
}
