import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //  switch
  bool checkBoxValue = false;
  bool val = false;

 
  bool switchControl = false;
  var textHolder = 'NOT RUNNING';

  void toggleSwitch(bool value) {
    if (switchControl == false) {
      setState(() {
        switchControl = true;
        textHolder = 'RUNNING';
      });
    } else {
      setState(() {
        switchControl = false;
        textHolder = 'NOT RUNNING';
      });
    }
  }

// switch
  bool ischecked = false; //check

  Map<String, bool> values = {
    'Stop 1': true,
    'Stop 2': false,
    'Stop 3': false,
    'Stop 4': false,
    'Stop 5': false,
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 10),
            child: pageView(context),
          ),
        ),
      ]),
    );
  }

  SingleChildScrollView pageView(context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Text(
                    '$textHolder',
                    style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.bold,
                        color: textHolder == "RUNNING"
                            ? Colors.green
                            : Colors.black87),
                  ),
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      Switch(
                        onChanged: toggleSwitch,
                        value: switchControl,
                        activeColor: textHolder == "RUNNING"
                            ? Colors.green
                            : Colors.black87,
                      ),
                      
                    ]),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    "1 hour 32 minutes",
                    style: TextStyle(fontWeight: FontWeight.w600),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    "Stop 2(2nd stop)",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                )
              ],
            ),
          ),
          Divider(),

          Container(
            width: double.infinity,
            
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 10, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Initial Location",
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15),
                      ),
                      Text(
                        "Final Location",
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 15),
                      ),
                    ],
                  ),
                ),
                Column(children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            "Stop 1",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            "Stop 5",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              color: Colors.black87,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ])
              ],
            ),
          ),
          // ),
          // ),
          Divider(),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: values.keys.map((String key) {
              return new CheckboxListTile(
                title: new Text(
                  key,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                ),
                value: values[key],
                dense: true,
                activeColor: Colors.black87,
                onChanged: (bool value) {
                  setState(() {
                    values[key] = value;
                  });
                },
              );
            }).toList(),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 25),
          )
        ],
      ),
    );
  }
}
